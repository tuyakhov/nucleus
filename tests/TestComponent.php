<?php


namespace nucleus\tests;


use nucleus\Component;
use nucleus\Html;

class TestComponent extends Component
{
    public function render()
    {
        return Html::tag('div', $this->props['amount']);
    }
}