<?php
namespace nucleus;

abstract class Component
{
    protected $props = [];
    protected $children;

    /**
     * Component constructor.
     * @param array $props
     * @param string|Component $children
     */
    public function __construct(array $props = [], $children = null)
    {
        $this->props = $props;
        $this->children = $children;
    }

    public function __toString()
    {
        $this->componentWillMount();
        $html = $this->render();
        $this->componentDidMount();
        return $html;
    }


    public function componentWillMount()
    {
    }

    public function componentDidMount()
    {
    }

    abstract public function render();
}