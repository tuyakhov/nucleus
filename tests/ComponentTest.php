<?php


namespace nucleus\tests;


use nucleus\Dom;
use PHPUnit\Framework\TestCase;

class ComponentTest extends TestCase
{
    public function testComponent()
    {
        $component = Dom::createElement(TestComponent::class, ['amount' => 3]);
        $this->assertSame((string) $component, '<div>3</div>');

        $component = Dom::createElement(TestComponent::class, [
            'amount' => Dom::createElement(TestComponent::class, ['amount' => 3])
        ]);
        $this->assertSame((string) $component, '<div><div>3</div></div>');
    }
}