<?php
namespace nucleus;

class Dom
{
    public static function createElement($type, $props = [], $children = null)
    {
        return new $type($props, $children);
    }
}